package hw5;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class DBCollection {
	
	private String name;
	private DB database;
	private File newFile;
	private HashMap<Integer, JsonObject> docMap;
	private int keyCount; 

	/**
	 * Constructs a collection for the given database
	 * with the given name. If that collection doesn't exist
	 * it will be created.
	 */
	public DBCollection(DB database, String name) {
		this.name = name;
		this.database = database;
		this.docMap = new HashMap<Integer, JsonObject>(); 
		this.keyCount = 0; 
		newFile = new File("testfiles/"+ database.name + "/" + name + ".json");
		try {
			Files.createFile(newFile.toPath());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Returns a cursor for all of the documents in
	 * this collection.
	 */
	public DBCursor find() {
		DBCursor cursor = new DBCursor(this);
		return cursor;
	}
	
	/**
	 * Finds documents that match the given query parameters.
	 * 
	 * @param query relational select
	 * @return
	 */
	public DBCursor find(JsonObject query) {
		DBCursor cursor = new DBCursor(this, query);
		return cursor;
	}
	
	/**
	 * Finds documents that match the given query parameters.
	 * 
	 * @param query relational select
	 * @param projection relational project
	 * @return
	 */
	public DBCursor find(JsonObject query, JsonObject projection) {
		DBCursor cursor = new DBCursor(this, query, projection);
		return cursor;
	}
	
	/**
	 * Inserts documents into the collection
	 * Must create and set a proper id before insertion
	 * When this method is completed, the documents
	 * should be permanently stored on disk.
	 * @param documents
	 */
	public void insert(JsonObject... documents) {
		//newFile
		for(JsonObject doc: documents) {
			try {
				docMap.put(keyCount, doc);
				Files.write(newFile.toPath(), Document.toJsonString(doc).getBytes(), StandardOpenOption.APPEND);
				keyCount++;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Locates one or more documents and replaces them
	 * with the update document.
	 * @param query relational select for documents to be updated
	 * @param update the document to be used for the update
	 * @param multi true if all matching documents should be updated
	 * 				false if only the first matching document should be updated
	 */
	public void update(JsonObject query, JsonObject update, boolean multi) {
		if(multi) {
			for(Map.Entry<Integer, JsonObject> entry : docMap.entrySet()) {
				if(entry.getValue().equals(query)) {
					docMap.put(entry.getKey(), update);
				}
			}
		}
		else {
			for(Map.Entry<Integer, JsonObject> entry : docMap.entrySet()) {
				if(entry.getValue().equals(query)) {
					docMap.put(entry.getKey(), update);
					break;
				}
			}
		}
		
		try {
			Files.write(newFile.toPath(), ("").getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.insert(docMap.values().toArray(new JsonObject[docMap.size()]));
	}
	
	/*
	 * Removes one or more documents that match the given
	 * query parameters
	 * @param query relational select for documents to be removed
	 * @param multi true if all matching documents should be updated
	 * 				false if only the first matching document should be updated
	 */
	public void remove(JsonObject query, boolean multi) {
		if(multi) {
			Iterator<Map.Entry<Integer, JsonObject>> iterator = docMap.entrySet().iterator();
			while(iterator.hasNext()) {
				Map.Entry<Integer, JsonObject> entry = iterator.next();
				if(entry.getValue().equals(query)) {
					iterator.remove();
				}
			}
		}
		else {
			for(Map.Entry<Integer, JsonObject> entry : docMap.entrySet()) {
				if(entry.getValue().equals(query)) {
					docMap.remove(entry.getKey());
					break;
				}
			}
		}
		
		try {
			Files.write(newFile.toPath(), ("").getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.insert(docMap.values().toArray(new JsonObject[docMap.size()]));
	}
	
	/**
	 * Returns the number of documents in this collection
	 */
	public long count() {
		return docMap.size();
	}
	
	public String getName() {
		return name;
	}
	
	/**
	 * Returns the ith document in the collection.
	 * Documents are separated by a line that contains only a single tab (\t)
	 * Use the parse function from the document class to create the document object
	 */
	public JsonObject getDocument(int i) {
		return docMap.get(i);
	}
	
	/**
	 * Drops this collection, removing all of the documents it contains from the DB
	 */
	public void drop() {
		try {
			docMap.clear();
			Files.delete(newFile.toPath());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
