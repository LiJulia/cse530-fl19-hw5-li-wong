package hw5;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.FileAttribute;

public class DB {
	
	public String name;
	private File newFile;

	/**
	 * Creates a database object with the given name.
	 * The name of the database will be used to locate
	 * where the collections for that database are stored.
	 * For example if my database is called "library",
	 * I would expect all collections for that database to
	 * be in a directory called "library".
	 * 
	 * If the given database does not exist, it should be
	 * created.
	 */
	public DB(String name) {
		this.name = name;
		newFile = new File("testfiles/" + name);
//		Path path = Paths.get(uri)
		try {
//			Files.copy(newFile.toPath(), newFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
//			FileAttribute<?> attrs = new FileAttribute<?>(name, "");
			Files.createDirectory(newFile.toPath());
		}
		catch(Exception e) {
			System.out.println("unable to create");
			e.printStackTrace();
		}
	}
	
	/**
	 * Retrieves the collection with the given name
	 * from this database. The collection should be in
	 * a single file in the directory for this database.
	 * 
	 * Note that it is not necessary to read any data from
	 * disk at this time. Those methods are in DBCollection.
	 */
	public DBCollection getCollection(String name) {
		DBCollection newCollection = new DBCollection(this, name);
		return newCollection;
	}
	
	/**
	 * Drops this database and all collections that it contains
	 */
	public void dropDatabase() {
		String[] files = newFile.list();
		for(String s: files) {
			File f = new File(newFile.getPath(), s);
			f.delete();
		}
		
		try {
			Files.delete(newFile.toPath());
		} catch (IOException e) {
			System.out.println("unable to drop");
			e.printStackTrace();
		}
	}
	
	
}
