package hw5;

import java.util.ArrayList;
import java.util.Iterator;

import com.google.gson.JsonObject;

public class DBCursor implements Iterator<JsonObject>{
	
	ArrayList<JsonObject> cursor = new ArrayList<JsonObject>();
	private DBCollection collection;
	private JsonObject query;
	private JsonObject fields;
	private Iterator<JsonObject> iterator;

	public DBCursor(DBCollection collection, JsonObject query, JsonObject fields) {
		this.collection = collection;
		this.query = query;
		this.fields = fields;
		for(int i = 0; i < collection.count(); i++) {
			if(query.equals(collection.getDocument(i)))
			{
				cursor.add(collection.getDocument(i));
				break;
			}
		}
		iterator = cursor.iterator();
	}
	
	public DBCursor(DBCollection collection) {
		this.collection = collection;
		for(int i = 0; i < collection.count(); i++) {
			cursor.add(collection.getDocument(i));
		}
		iterator = cursor.iterator();
	}
	
	public DBCursor(DBCollection collection, JsonObject query) {
		this.collection = collection;
		this.query = query;
		for(int i = 0; i < collection.count(); i++) {
			if(query.equals(collection.getDocument(i))) {
				cursor.add(collection.getDocument(i));
			}
		}
		iterator = cursor.iterator();
	}
	
	/**
	 * Returns true if there are more documents to be seen
	 */
	public boolean hasNext() {
		while(iterator.hasNext()) {
			return true;
		}
		return false;
	}

	/**
	 * Returns the next document
	 */
	public JsonObject next() {
		return iterator.next();
	}
	
	/**
	 * Returns the total number of documents
	 */
	public long count() {
		return cursor.size();
	}

}
