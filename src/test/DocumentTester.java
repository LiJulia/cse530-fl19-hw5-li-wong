package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.google.gson.JsonObject;

import hw5.Document;

class DocumentTester {
	
	/*
	 * Things to consider testing:
	 * 
	 * Invalid JSON 
	 * 
	 * Properly parses embedded documents
	 * Properly parses arrays
	 * Properly parses primitives (done!)
	 * 
	 * Object to embedded document
	 * Object to array
	 * Object to primitive (done!)
	 *
	 */
	
	@Test
	public void testParsePrimitive() {
		String json = "{\"key\":\"value\"}";//setup
		JsonObject results = Document.parse(json); //call method to be tested
		assertTrue(results.getAsJsonPrimitive("key").getAsString().equals("value")); //verify results
	}
	
	@Test
	public void testToJsonStringPrimitive() {
		JsonObject obj = new JsonObject();
		obj.addProperty("key", "value");
		String newString = Document.toJsonString(obj);
		assertTrue(newString.equals("{\"key\":\"value\"}"));
	}
	
	@Test
	public void testParseArrayDocument() {
		String json = "{\"array\":[\"one\",\"two\",\"three\"]}"; //setup
		JsonObject results = Document.parse(json); //call method to be tested
		assertTrue(results.get("array").toString().equals("[\"one\",\"two\",\"three\"]")); //verify results
	}
	
	@Test
	public void testToJsonStringArray() {
		JsonObject obj = new JsonObject();
		obj.addProperty("key", "value");
		obj.addProperty("name", "kevin");
		String newString = Document.toJsonString(obj);
		assertTrue(newString.equals("{\"key\":\"value\",\"name\":\"kevin\"}"));
	}
	
	@Test
	public void testParseEmbeddedDocument() {
		String json = "{\"embedded\":{\"key2\":\"value2\"}}"; 
		JsonObject results = Document.parse(json); 
		assertTrue(results.get("embedded").toString().equals("{\"key2\":\"value2\"}")); 
	}
	
}
