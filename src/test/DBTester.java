package test;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;

import org.junit.jupiter.api.Test;

import hw5.DB;

class DBTester {
	
	/**
	 * Things to consider testing:
	 * 
	 * Properly creates directory for new DB (done)
	 * Properly accesses existing directory for existing DB
	 * Properly accesses collection
	 * Properly drops a database
	 * Special character handling?
	 */
	
	@Test
	public void testCreateDB() {
		DB hw5 = new DB("hw5"); //call method
		assertTrue(new File("testfiles/hw5").exists()); //verify results
		hw5.dropDatabase();
	}

	@Test
	public void testAccessCollection() {
		DB hw5 = new DB("test");
		assertTrue(hw5.getCollection("test").getName().equals("test"));
		hw5.dropDatabase();
	}
	
	@Test
	public void testDropDatabase() {
		DB hw5 = new DB("test2");
		File newFile = new File("testfiles/test2");
		hw5.dropDatabase();
		assertFalse(newFile.exists());
	}
}
