package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.google.gson.JsonObject;

import hw5.DB;
import hw5.DBCollection;
import hw5.DBCursor;
import hw5.Document;

class CursorTester {
	
	/**
	 * Things to consider testing:
	 * 
	 * hasNext (done?)
	 * count (done?)
	 * next (done?)
	 */

	@Test
	public void testFindAll() {
		DB db = new DB("cursortest");
		DBCollection test = db.getCollection("ctest");
		JsonObject obj = new JsonObject();
		obj.addProperty("key", "value");
		test.insert(obj);
		JsonObject obj2 = new JsonObject();
		obj2.addProperty("key2", "value2");
		test.insert(obj2);
		JsonObject obj3 = new JsonObject();
		obj3.addProperty("key3", "value3");
		test.insert(obj3);
		
		DBCursor results = test.find();
		
		assertTrue(results.count() == 3);
		assertTrue(results.hasNext());
		JsonObject d1 = results.next(); //pull first document
		//verify contents?
		assertTrue(results.hasNext());//still more documents
		JsonObject d2 = results.next(); //pull second document
		//verfiy contents?
		assertTrue(results.hasNext()); //still one more document
		JsonObject d3 = results.next();//pull last document
		assertFalse(results.hasNext());//no more documents
		db.dropDatabase();
	}

	@Test
	public void testFindWithQuery() {
		DB db = new DB("cursortest2");
		DBCollection test = db.getCollection("ctest2");
		JsonObject obj = new JsonObject();
		obj.addProperty("key", "value");
		test.insert(obj);
		JsonObject obj2 = new JsonObject();
		obj2.addProperty("key2", "value2");
		test.insert(obj2);
		test.insert(obj2);
		JsonObject obj3 = new JsonObject();
		obj3.addProperty("key3", "value3");
		test.insert(obj3);
		
		DBCursor results = test.find(obj2);
		
		assertTrue(results.count() == 2);
		assertTrue(results.hasNext());
		db.dropDatabase();
	}
	
	@Test
	public void testFindWithProject() {
		DB db = new DB("cursortest3");
		DBCollection test = db.getCollection("ctest3");
		JsonObject obj = new JsonObject();
		obj.addProperty("key", "value");
		test.insert(obj);
		JsonObject obj2 = new JsonObject();
		obj2.addProperty("key2", "value2");
		test.insert(obj2);
		test.insert(obj2);
		JsonObject obj3 = new JsonObject();
		obj3.addProperty("key3", "value3");
		test.insert(obj3);
		
		DBCursor results = test.find(obj2, obj);
		
		assertTrue(results.count() == 1);
		assertTrue(results.hasNext());
		db.dropDatabase();
	}
}
