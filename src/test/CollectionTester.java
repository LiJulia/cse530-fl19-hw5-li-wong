package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.google.gson.JsonObject;

import hw5.DB;
import hw5.DBCollection;
import hw5.DBCursor;

class CollectionTester {
	
	/**
	 * Things to consider testing
	 * 
	 * Queries:
	 * 	Find all
	 * 	Find with relational select
	 * 		Conditional operators
	 * 		Embedded documents
	 * 		Arrays
	 * 	Find with relational project
	 * 
	 * Inserts (done?)
	 * Updates (done?)
	 * Deletes
	 * 
	 * getDocument (done?)
	 * drop
	 */
	
	@Test
	public void testGetDocument() {
		DB db = new DB("sprite");
		DBCollection test = db.getCollection("spritetest");
		JsonObject obj = new JsonObject();
		obj.addProperty("key", "value");
		test.insert(obj);
		JsonObject primitive = test.getDocument(0);
		assertTrue(primitive.getAsJsonPrimitive("key").getAsString().equals("value"));
		db.dropDatabase();
	}
	
	@Test
	public void testInsert() {
		DB db = new DB("sprite");
		DBCollection test = db.getCollection("spritetest");
		assertTrue(test.count() == 0); 
		JsonObject obj = new JsonObject();
		obj.addProperty("key", "value");
		test.insert(obj);
		assertTrue(test.count() == 1); 
		JsonObject obj2 = new JsonObject();
		obj2.addProperty("key2", "value2");
		test.insert(obj2);
		assertTrue(test.count() == 2); 
		db.dropDatabase();
	}
	
	@Test
	public void testUpdatePrimitiveFirst() {
		DB db = new DB("sprite");
		DBCollection test = db.getCollection("spritetest");
		JsonObject obj = new JsonObject();
		obj.addProperty("key", "value");
		test.insert(obj);
		JsonObject obj2 = new JsonObject();
		obj2.addProperty("key2", "value2");
		test.update(obj, obj2, false);
		JsonObject primitive = test.getDocument(0);
		assertTrue(primitive.getAsJsonPrimitive("key2").getAsString().equals("value2"));
		db.dropDatabase();
	}
	
	@Test
	public void testUpdatePrimitiveAll() {
		DB db = new DB("sprite");
		DBCollection test = db.getCollection("spritetest");
		JsonObject obj = new JsonObject();
		obj.addProperty("key", "value");
		test.insert(obj);
		test.insert(obj);
		JsonObject obj2 = new JsonObject();
		obj2.addProperty("key2", "value2");
		test.update(obj, obj2, true);
		JsonObject primitive = test.getDocument(0);
		assertTrue(primitive.getAsJsonPrimitive("key2").getAsString().equals("value2"));
		JsonObject primitive2 = test.getDocument(1);
		assertTrue(primitive2.getAsJsonPrimitive("key2").getAsString().equals("value2"));
		db.dropDatabase();
	}
	
	@Test
	public void testDropPrimitive() {
		DB db = new DB("dropTest");
		DBCollection collection = db.getCollection("drop");
		JsonObject obj = new JsonObject();
		obj.addProperty("key", "value");
		collection.insert(obj);
		assertTrue(collection.count() == 1); 
		collection.drop(); 
		assertTrue(collection.count() == 0);
		db.dropDatabase();
	}
	
	@Test
	public void testRemoveMultiTrue() {
		DB db = new DB("spriteRemove");
		DBCollection test = db.getCollection("spriteTestRemove");
		JsonObject obj = new JsonObject();
		obj.addProperty("key", "value");
		test.insert(obj);
		JsonObject obj2 = new JsonObject();
		obj2.addProperty("key2", "value2");
		test.insert(obj2);
		test.insert(obj2);
		test.remove(obj2, true);
		JsonObject primitive = test.getDocument(0);
		assertTrue(primitive.getAsJsonPrimitive("key").getAsString().equals("value"));
		try {
		test.getDocument(1);
		}
		catch (Exception e) {
			assertTrue(true); 
		}
		try {
		test.getDocument(2);
		}
		catch (Exception e) {
			assertTrue(true); 
		}
		db.dropDatabase();
		
	}
	
	@Test
	public void testRemoveMultiFalse() {
		DB db = new DB("spriteRemoveF");
		DBCollection test = db.getCollection("spriteTestRemoveF");
		JsonObject obj = new JsonObject();
		obj.addProperty("key", "value");
		test.insert(obj);
		JsonObject obj2 = new JsonObject();
		obj2.addProperty("key2", "value2");
		test.insert(obj2);
		test.insert(obj2);
		test.remove(obj2, false);
		JsonObject primitive = test.getDocument(0);
		assertTrue(primitive.getAsJsonPrimitive("key").getAsString().equals("value"));
		JsonObject primitive1 = test.getDocument(2);
		assertTrue(primitive1.getAsJsonPrimitive("key2").getAsString().equals("value2"));
		try {
		test.getDocument(1);
		}
		catch (Exception e) {
			assertTrue(true); 
		}
		db.dropDatabase();
	}
	
}
